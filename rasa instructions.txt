python3.6 -m venv --system-site-packages rasa_env		# python3.6 - must, not python 3.7
$ source ./rasa_env/bin/activate

$ pip install rasa
# run sample chat
$ rasa init

# domain.yml
# data/stories.md
# endpoints.yml

$ pip install rasa_sdk
# actions.py

